package com.example.demo;

public class Account {
	String OwnerName;
	String Address_City;
	String State;
	String Pin;
	int BalanceAmount;
	int doc;
	public String getOwnerName() {
		return OwnerName;
	}
	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}
	public String getAddress_City() {
		return Address_City;
	}
	public void setAddress_City(String address_City) {
		Address_City = address_City;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getPin() {
		return Pin;
	}
	public void setPin(String pin) {
		Pin = pin;
	}
	public int getBalanceAmount() {
		return BalanceAmount;
	}
	public void setBalanceAmount(int balanceAmount) {
		BalanceAmount = balanceAmount;
	}
	public int getDoc() {
		return doc;
	}
	public void setDoc(int doc) {
		this.doc = doc;
	}
	
}
